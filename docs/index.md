Kind: Statement of Work
Title: <Task or Project Name>
Author: Hilton Lipschitz
Affiliation: <Company Name>
Date: 2016-03-04
Version: 0.1


<The deliverables of the project, so we know when it is complete>
# 1. Project Title: [Advanced Hydroponic System;Fully automated]

<p>
As we all know this years theme is sustainable tourism,and this sector has a big impact on our economy. To do our research we had a talk with mister Noah Afata ,lodgeholder of Danpaati. We talked about the different problems they had such as recycling ,clean water etc.But he said that fresh fruits and vegetables where a concern because they get their fruits and vegetables from Paramaribo and usually it is not fresh anymore.
</p>
<p>
As a team we then decided to build something that could be used to grow vegetables and fruits. We stumbled across a hydroponic system but we wanted to add IOT to it so it can be as advanced as possible. That's why we present to you Advanced Hydroponic system;Fully Automated.
</p>
![alt_text](https://snipboard.io/VtgKky.jpg "image_tooltip")
## Overview & Features

<p>
There is a Dashboard/User interface with the measurements taken by the sensors such as the electrical Conductivity meter,DS18B20 waterproof temperature meter and the liliygo T-higrow sensor.
In concept we have a peristaltic pump that ,with a push of a button on the Dashboard, it can automatically send nutrients to the pvc pipes.
</p>
<p>
When power cuts off ,a hydroponics system can survive only 30 minutes without power. That's why we created an ATS (automatic transfer switch)which automatically turn on a back up battery
</p>

## Demo / Proof of work

<p>
-Working Demo<a href="https://photos.google.com/share/AF1QipPTIy4LQ-xPk9VYlP3ku4wvSrufxHXnqpMUWzYs6GaMQovsa5R0RLoIl-stk_AcCA/photo/AF1QipNr4zO9HAN4VKKHy6_lgSQOsg3lPmgRPwOhr7sz?key=a1RDVndjVTVoWm5yb2NjUHppcHV6ZGRMUUZOLTZ3"> Link to our demo video</a>
</p>


## Deliverables

<p>
-Documenation Video<a href="https://drive.google.com/file/d/1ByByIkRxywWWSghxgkNeDBJmZBA73YNM/view"> Link to our  documentation video</a>
</p>
<p>
-Pitch<a href="https://docs.google.com/presentation/d/1Y1ZjilFAPPV_zMDtf2mg84nrMK9EBUb8/edit#slide=id.p1"> Link to our  pitch</a>
</p>
<p>
-Project Poster<a href="https://drive.google.com/file/d/1BJnARL_TO2M-uHG90Zgnl2F5d_IinVFs/view"> Link to our  Project poster</a>
</p>

## Project landing page/website (optional)

[https://clever-banach-3dd700.netlify.app](https://clever-banach-3dd700.netlify.app)

## the team & contact

<p>
Hi ,we are Code-Docs.The team consists of 3 members . 
</p>
<ul>
 
<li>Landveld Shyfka -3th year student of NATIN,application developer
contact:+597 889-1716
<li>Overeem Shayenne -3th year student of NATIN, application developer
contact:+597 868-4205
<li>Santai Gerson-4th year student of NATIN, electrical engineering
contact:+597 747-7328
</li>
</ul>

