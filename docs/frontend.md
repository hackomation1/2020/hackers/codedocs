# 3. Front-End & Platform


Our team decided to use the ESP32 and connect to the internet. We now could develop a dashboard. We added the lilygo T-higrow sensor at last so thats why we have two dashboards but we can make it one.


## 3.1 Objectives

We chose this so that the customer can see the different measurements taken with the sensors such as temperature and electrical conductivity. He can also see the local time.

Steps that need to be taken are:



*   Coding in Arduino IDE
*   Connecting wires of sensor to our ESP32 
*   Uploading code to board
*   Testing dashboard

## 3.3 Steps taken
#  Coding in Arduino IDE

![alt_text](https://snipboard.io/76EDlz.jpg "image_tooltip")


#  Connecting wires of sensor to our ESP32 

![alt_text](https://snipboard.io/gMsyWc.jpg "image_tooltip")

#  Uploading code to board

![alt_text](https://snipboard.io/lLw1BU.jpg "image_tooltip")

#  Testing dashboard

![alt_text](https://snipboard.io/0VOYml.jpg "image_tooltip")



## 3.4 Testing & Problems

The only problem we had here was the part where our IP adress did not show up because our baud rate was on 921600 but our code had 115200 in it. We changed the rate and it worked perfectly.

## 3.5 Proof of work

[https://drive.google.com/file/d/1hOatKJtqRWlz7J6wwPO50PMYOzwqqinF/view?usp=sharing](https://drive.google.com/file/d/1hOatKJtqRWlz7J6wwPO50PMYOzwqqinF/view?usp=sharing)


## 3.7 References & Credits

[https://github.com/Xinyuan-LilyGO/TTGO-HiGrow](https://github.com/Xinyuan-LilyGO/TTGO-HiGrow)

[https://randomnerdtutorials.com/esp32-ds18b20-temperature-arduino-ide/#:~:text=Introducing%20DS18B20%20Temperature%20Sensor&text=Each%20DS18B20%20temperature%20sensor%20has,also%20available%20in%20waterproof%20version](https://randomnerdtutorials.com/esp32-ds18b20-temperature-arduino-ide/#:~:text=Introducing%20DS18B20%20Temperature%20Sensor&text=Each%20DS18B20%20temperature%20sensor%20has,also%20available%20in%20waterproof%20version).


