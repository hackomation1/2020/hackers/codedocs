# 2. Embedded solution

The embedded solution we went for was making sure that the customer knows more about his plants and can control his system from afar.

## 2.1 Objectives

# What are we solving?

The problem we are trying to solve is to help the tourism sector in the inland but also make farming easy and sustainable as possible. 


# Why are we solving this?

We chose hydroponics because there are many benefits to hydroponics of growing your own plants sustainably at home. The plant is grown directly on water, which makes the use of soil superfluous. The nutrients are dissolved in the water so that they can be absorbed much more efficiently and the plants grow faster as a result. In no time, your own basil, red pepper or tomato plant will be within reach. In the living room, on the balcony or in the backyard, there is room for hydroponics in every home. Going to the supermarket for your fresh herbs will soon be superfluous. And good news: it takes very little effort!


# Steps 



*   Build system
*   Code Sensors
*   Add sensors to system


## 2.3 Steps taken

<go through the proposed steps and describe and add images>
# Building Hydroponic system 

On the 7th of november we officially began building the system. 



After Soldering the Esp32, we tested the functionality and the ability of the water pump. 

Day 2 of Building Hydroponic System

On the day of 11th november 2020 we made the holes in the pipes we are using for the system. We finished the first version of programming the Waterproof Temperature sensor en de Ec Meter. 


We also had a discussion
The discussion was about getting a notification when power cuts out. A hydroponics system survives only 30 minutes after a power failure. So, we decided to use a magnetic switch. A red led will start blinking after a several seconds when the power cuts out. 

Day 3 of Building Hydroponic System

On 14th of november we checked how the peristaltic pump worked and how much water went in each individual pvc pipe. Gerson also worked on the stelation of our system 




![alt_text](https://snipboard.io/ptZAwb.jpg "image_tooltip")



## 2.4 Testing & Problems

<provide tests performed, describe problems encountered and how they were solved (or not)>
The first thing we did was get info about the different sensors we had. We had the ESP32 board, an analog electrical conductivity sensor , the Ds18b20 waterproof temperature sensor ,ESP32 board and the lilygo T-higrow sensor
![alt_text](https://snipboard.io/AVwNcy.jpg "image_tooltip")
![alt_text](https://snipboard.io/LnrjAg.jpg "image_tooltip")
![alt_text](https://snipboard.io/lT35gy.jpg "image_tooltip")


We then proceeded to get some code on the internet to see if they worked.

On the 28th of october we tried our dashboard for the temperature sensor and Ec meter. As you can see ,it also tells you the current time.

          

Day 2 of coding

After the coding of our dashboard we went on to our peristaltic pump. We got our pump enclosure on 14th of november.

 It was then time to go code the pump to send nutrients to the plant,but we needed to find the perfect pipe , it took us a while but we did find it.

After a while we did get the pump to work but then it suddenly stopped working and we didn’t had the time to actually fix this.

Lastly we have the Lilygo T-higrow sensor. This sensor was used by our team member Shayenne for her Inno starter project where she made an Mini IOT greenhouse. With this sensor ,you can measure



*    the light intensity 
*   the humidthy 
*   the temperature 
*   the soil 
*   and if you’re using a battery ,it also measures the battery life.

 It has a local dashboard but if you have multiple sensors, you can change the code to take multiple measurements.


## 2.5 Proof of work
[Video of the pump working](https://drive.google.com/file/d/1ralQzMPGN9Du5xF4mNcIGa_Cguo2dbRz/view?usp=sharing)
[Link video Demonstration sensors](https://drive.google.com/file/d/1k879HhZ5REkh-scCSlGfFw2hJW2qSUNv/view?usp=sharing)



